CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainer


INTRODUCTION
------------

This module provides a basic user interface for making payments via the
OutVoice Platform.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/outvoice

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/outvoice


REQUIREMENTS
------------

 * You must have an account at https://outvoice.com
 * You must have the OutVoice module enabled


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

You can choose which content types will include OutVoice payment options
and control various other parameters at /admin/configuration/outvoice.


MAINTAINERS
-----------

Current maintainer:
 * Issa Diao (mr_issa) - https://www.drupal.org/u/mr_issa
